var gulp = require("gulp"),
    ts = require("gulp-typescript"),
    browserify = require("browserify"),
    source = require("vinyl-source-stream"),
    buffer = require("vinyl-buffer"),
    uglify = require("gulp-uglify"),
    gutil = require("gulp-util"),
    sourcemaps = require('gulp-sourcemaps'),
    del = require("del"),
    rename = require("gulp-rename"),
    merge = require("merge2");

gulp.task('compile-ts', function() {
    var tsResult = gulp.src(['typings/**/!(northwind-communicator)/*.d.ts', 'src/**/*.ts'])
                        .pipe(sourcemaps.init()) // This means sourcemaps will be generated
                        .pipe(ts({
                           declarationFiles: true,
                           noExternalResolve: true,
                           module: "commonjs",
                           outDir: "typings/northwind-communicator"
                        }));
    return merge(
            tsResult.dts.pipe(gulp.dest('typings/northwind-communicator')),
            tsResult.js
                .pipe(sourcemaps.write('.')) // Now the sourcemaps are added to the .js file
                .pipe(gulp.dest('js')));
});

gulp.task("browserify", ["compile-ts"], function() {
    var bundler = browserify("js/northwind-communicator.js", {
        debug: false,
        standalone: "NorthwindCommunicator"
    }).bundle();

    // generate library file and  minified file
    return bundler
        .pipe(source("northwind-communicator.js"))
        .pipe(gulp.dest("dist"));
});

gulp.task("minify", ["browserify"], function() {
    return gulp.src("dist/northwind-communicator.js")
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .on("error", gutil.log)
        .pipe(rename(function (path) {
                if(path.extname === '.js') {
                    path.basename += '.min';
                }
            }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("dist"));
});

gulp.task("default", ["minify"]);
