# Northwind Communicator

This is a sample project to demonstrate the end user experience in consuming a TypeScript library without having any custom setup on the end machine.

In this use case, the TypeScript files as well as the compiled and minified distributables are checked into source control. So, they can be consumed directly without running any custom build processes.

If interested in developing the library, please follow the instructions below.

## Installation
npm install -g bower superstatic tsd@next  
npm install  
bower install  
tsd update -so  

## Development
gulp

## Start server
superstatic
