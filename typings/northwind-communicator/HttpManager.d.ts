/// <reference path="../jquery/jquery.d.ts" />
declare class HttpManager {
    http<T>(url: string, data: any, method?: string): JQueryPromise<T>;
}
export = HttpManager;
