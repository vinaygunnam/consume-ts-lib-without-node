declare class NorthwindManager {
    private httpManager;
    private northwindEndpoint;
    constructor();
    getCustomers(): JQueryPromise<ICustomer[]>;
}
export = NorthwindManager;
