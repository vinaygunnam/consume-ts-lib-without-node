/// <reference path="NorthwindManager.d.ts" />
declare class NorthwindCommunicator {
    private static northwindManager;
    static getCustomers(): JQueryPromise<ICustomer[]>;
}
export = NorthwindCommunicator;
