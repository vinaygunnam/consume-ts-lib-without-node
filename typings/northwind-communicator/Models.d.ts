interface ICustomer {
    CustomerID: string;
    CompanyName: string;
    ContactName: string;
    ContactTitle: string;
}
