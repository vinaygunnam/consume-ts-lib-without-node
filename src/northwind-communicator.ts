/// <reference path="./NorthwindManager.ts"/>
import NorthwindManager = require("./NorthwindManager");

class NorthwindCommunicator {
    private static northwindManager: NorthwindManager = new NorthwindManager();

    static getCustomers(): JQueryPromise<ICustomer[]> {
        return NorthwindCommunicator.northwindManager.getCustomers();
    }
}

export = NorthwindCommunicator;
