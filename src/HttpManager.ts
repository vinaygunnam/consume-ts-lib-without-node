/// <reference path="../typings/jquery/jquery.d.ts"/>
class HttpManager {
    http<T>(url: string, data: any, method: string = "POST", mockMode: boolean = false): JQueryPromise<T> {
        if (!url) return null;

        if (mockMode) {
            if (url.indexOf("customers") > -1) {
                url = "/mock-data/customers.json";
                method = "GET";
            }
        }

        switch (method) {
            case "GET":
            case "DELETE":
                if (data) {
                    url = url + $.param(data);
                    data = null
                }
                break;
            case "POST":
            case "PUT":
            case "PATCH":
                break;
        }

        return $.ajax({
            url: url,
            type: method,
            data: data
        });
    }
}

export = HttpManager;
