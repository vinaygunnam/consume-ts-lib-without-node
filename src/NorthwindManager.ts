import HttpManager = require("./HttpManager");

class NorthwindManager {
    private httpManager: HttpManager = new HttpManager();
    private northwindEndpoint: string = "http://services.odata.org/V4/Northwind/Northwind.svc/";

    constructor() {}

    getCustomers(): JQueryPromise<ICustomer[]> {
        return this.httpManager.http(this.northwindEndpoint + "Customers", null, "GET");
    }
}

export = NorthwindManager;
