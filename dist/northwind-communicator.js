(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.NorthwindCommunicator = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/// <reference path="../typings/jquery/jquery.d.ts"/>
var HttpManager = (function () {
    function HttpManager() {
    }
    HttpManager.prototype.http = function (url, data, method) {
        if (method === void 0) { method = "POST"; }
        if (!url)
            return null;
        switch (method) {
            case "GET":
            case "DELETE":
                if (data) {
                    url = url + $.param(data);
                    data = null;
                }
                break;
            case "POST":
            case "PUT":
            case "PATCH":
                break;
        }
        return $.ajax({
            url: url,
            type: method,
            data: data
        });
    };
    return HttpManager;
})();
module.exports = HttpManager;

//# sourceMappingURL=HttpManager.js.map
},{}],2:[function(require,module,exports){
var HttpManager = require("./HttpManager");
var NorthwindManager = (function () {
    function NorthwindManager() {
        this.httpManager = new HttpManager();
        this.northwindEndpoint = "http://services.odata.org/V4/Northwind/Northwind.svc/";
    }
    NorthwindManager.prototype.getCustomers = function () {
        return this.httpManager.http(this.northwindEndpoint + "Customers", null, "GET");
    };
    return NorthwindManager;
})();
module.exports = NorthwindManager;

//# sourceMappingURL=NorthwindManager.js.map
},{"./HttpManager":1}],3:[function(require,module,exports){
/// <reference path="./NorthwindManager.ts"/>
var NorthwindManager = require("./NorthwindManager");
var NorthwindCommunicator = (function () {
    function NorthwindCommunicator() {
    }
    NorthwindCommunicator.getCustomers = function () {
        return NorthwindCommunicator.northwindManager.getCustomers();
    };
    NorthwindCommunicator.northwindManager = new NorthwindManager();
    return NorthwindCommunicator;
})();
module.exports = NorthwindCommunicator;

//# sourceMappingURL=northwind-communicator.js.map
},{"./NorthwindManager":2}]},{},[3])(3)
});